#Boa:Dialog:AboutDialog

import wx

def create(parent):
    return AboutDialog(parent)

[wxID_ABOUTDIALOG, wxID_ABOUTDIALOGCLOSEBUTTON, wxID_ABOUTDIALOGSTATICBITMAP1,
 wxID_ABOUTDIALOGSTATICTEXT1, wxID_ABOUTDIALOGSTATICTEXT2,
 wxID_ABOUTDIALOGSTATICTEXT3,
] = [wx.NewId() for _init_ctrls in range(6)]

class AboutDialog(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_ABOUTDIALOG, name=u'AboutDialog',
              parent=prnt, pos=wx.Point(456, 209), size=wx.Size(373, 328),
              style=wx.CAPTION, title=u'About...')
        self.SetClientSize(wx.Size(365, 294))
        self.SetIcon(wx.Icon("bitmaps/MsgExplorer.ico",wx.BITMAP_TYPE_ICO))

        self.CloseButton = wx.Button(id=wxID_ABOUTDIALOGCLOSEBUTTON,
              label=u'Close', name=u'CloseButton', parent=self,
              pos=wx.Point(272, 256), size=wx.Size(75, 23), style=0)
        self.CloseButton.SetDefault()
        self.CloseButton.Bind(wx.EVT_BUTTON, self.OnCloseButtonButton,
              id=wxID_ABOUTDIALOGCLOSEBUTTON)

        self.staticBitmap1 = wx.StaticBitmap(bitmap=wx.Bitmap(u'bitmaps/logo.gif',
              wx.BITMAP_TYPE_GIF), id=wxID_ABOUTDIALOGSTATICBITMAP1,
              name='staticBitmap1', parent=self, pos=wx.Point(71, 0),
              size=wx.Size(222, 79), style=0)
        self.staticBitmap1.Center(wx.HORIZONTAL)

        self.staticText1 = wx.StaticText(id=wxID_ABOUTDIALOGSTATICTEXT1,
              label=u'Message Explorer', name='staticText1', parent=self,
              pos=wx.Point(64, 112), size=wx.Size(233, 30), style=0)
        self.staticText1.SetFont(wx.Font(20, wx.SWISS, wx.NORMAL, wx.BOLD,
              False, u'Forte'))

        self.staticText2 = wx.StaticText(id=wxID_ABOUTDIALOGSTATICTEXT2,
              label=u'Author : C\xe9dric RICARD <ricard@softarchi.com>',
              name='staticText2', parent=self, pos=wx.Point(40, 200),
              size=wx.Size(236, 13), style=0)

        self.staticText3 = wx.StaticText(id=wxID_ABOUTDIALOGSTATICTEXT3,
              label=u'Version : 1.7', name='staticText3', parent=self,
              pos=wx.Point(40, 184), size=wx.Size(65, 13), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)

    def OnCloseButtonButton(self, event):
        self.Close()
