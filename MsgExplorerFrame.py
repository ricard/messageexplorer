#Boa:Frame:MsgExplorerFrame

import wx
import wx.gizmos
import muscle
import sys, os
import struct
import traceback
import base64
import StringIO
import AboutDialog

def create(parent):
    return MsgExplorerFrame(parent)

def DisplayTrace(txtCtrl, detail = ''):
    txtCtrl.AppendText(str(sys.exc_info()[0]) + ' : ' + str(detail) + '\n')
    txtCtrl.AppendText('   Traceback:\n')
    tb = sys.exc_info()[2]
    list = traceback.format_tb(tb)
    for line in list:
        txtCtrl.AppendText(line)

[wxID_MSGEXPLORERFRAME, wxID_MSGEXPLORERFRAMELOGTEXTCTRL,
 wxID_MSGEXPLORERFRAMEMAINSPLITTER, wxID_MSGEXPLORERFRAMEMSGTREE,
 wxID_MSGEXPLORERFRAMESTATUSBAR, wxID_MSGEXPLORERFRAMETOOLBAR1,
] = map(lambda _init_ctrls: wx.NewId(), range(6))

friendlyTypes = {muscle.B_ANY_TYPE:     'Any type ??',
                 muscle.B_BOOL_TYPE:    'Boolean',
                 muscle.B_DOUBLE_TYPE:  'Double',
                 muscle.B_FLOAT_TYPE:   'Float',
                 muscle.B_INT64_TYPE:   'Int64',
                 muscle.B_INT32_TYPE:   'Int32',
                 muscle.B_INT16_TYPE:   'Int16',
                 muscle.B_INT8_TYPE:    'Int8',
                 muscle.B_UINT64_TYPE: 'UInt64',
                 muscle.B_UINT32_TYPE: 'UInt32',
                 muscle.B_UINT16_TYPE: 'UInt16',
                 muscle.B_UINT8_TYPE:  'UInt8',
                 muscle.B_MESSAGE_TYPE: 'Message',
                 muscle.B_ARCHIVE_TYPE:'Archive',
                 muscle.B_POINTER_TYPE: 'Pointer',
                 muscle.B_POINT_TYPE:   'Point',
                 muscle.B_RECT_TYPE:    'Rectangle',
                 muscle.B_STRING_TYPE:  'String',
                 muscle.B_OBJECT_TYPE:  'Object',
                 muscle.B_RAW_TYPE:     'Raw data'}

[wxID_MSGEXPLORERFRAMEFILEMENUOPEN, wxID_MSGEXPLORERFRAMEFILEMENUQUIT,
] = map(lambda _init_coll_fileMenu_Items: wx.NewId(), range(2))

[wxID_MSGEXPLORERFRAMETOOLBAR1ID_OPEN] = map(lambda _init_coll_toolBar1_Tools: wx.NewId(), range(1))

[wxID_MSGEXPLORERFRAMEHELPMENUABOUT] = map(lambda _init_coll_helpMenu_Items: wx.NewId(), range(1))

[wxID_MSGEXPLORERFRAME, wxID_MSGEXPLORERFRAMELOGTEXTCTRL,
 wxID_MSGEXPLORERFRAMEMAINSPLITTER, wxID_MSGEXPLORERFRAMEMSGTREE,
 wxID_MSGEXPLORERFRAMESTATUSBAR, wxID_MSGEXPLORERFRAMETOOLBAR1,
] = [wx.NewId() for _init_ctrls in range(6)]

[wxID_MSGEXPLORERFRAMEFILEMENUOPEN, wxID_MSGEXPLORERFRAMEFILEMENUQUIT,
] = [wx.NewId() for _init_coll_fileMenu_Items in range(2)]

[wxID_MSGEXPLORERFRAMETOOLBAR1ID_OPEN] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_MSGEXPLORERFRAMEHELPMENUABOUT] = [wx.NewId() for _init_coll_helpMenu_Items in range(1)]

class MsgExplorerFrame(wx.Frame):
    def _init_coll_mainFrameSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.mainSplitter, 1, border=0, flag=wx.GROW)

    def _init_coll_menuBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.fileMenu, title=u'File')
        parent.Append(menu=self.helpMenu, title=u'Help')

    def _init_coll_helpMenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'About MessageExplorer',
              id=wxID_MSGEXPLORERFRAMEHELPMENUABOUT, kind=wx.ITEM_NORMAL,
              text=u'About')
        self.Bind(wx.EVT_MENU, self.OnHelpMenuAboutMenu,
              id=wxID_MSGEXPLORERFRAMEHELPMENUABOUT)

    def _init_coll_fileMenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help=u'Open a Flattened Message',
              id=wxID_MSGEXPLORERFRAMEFILEMENUOPEN, kind=wx.ITEM_NORMAL,
              text=u'Open\tCTRL+O')
        parent.AppendSeparator()
        parent.Append(help=u'Quit MessageExplorer',
              id=wxID_MSGEXPLORERFRAMEFILEMENUQUIT, kind=wx.ITEM_NORMAL,
              text=u'Quit\tCTRL+Q')
        self.Bind(wx.EVT_MENU, self.OnOpen,
              id=wxID_MSGEXPLORERFRAMEFILEMENUOPEN)
        self.Bind(wx.EVT_MENU, self.OnFileMenuQuitMenu,
              id=wxID_MSGEXPLORERFRAMEFILEMENUQUIT)

    def _init_coll_toolBar1_Tools(self, parent):
        # generated method, don't edit

        parent.DoAddTool(bitmap=wx.Bitmap(u'bitmaps/open.gif',
              wx.BITMAP_TYPE_ANY), bmpDisabled=wx.NullBitmap,
              id=wxID_MSGEXPLORERFRAMETOOLBAR1ID_OPEN, kind=wx.ITEM_NORMAL,
              label=u'Open Message file', longHelp=u'',
              shortHelp=u'Open Message file')
        self.Bind(wx.EVT_TOOL, self.OnOpen,
              id=wxID_MSGEXPLORERFRAMETOOLBAR1ID_OPEN)

        parent.Realize()

    def _init_coll_msgTree_Columns(self, parent):
        # generated method, don't edit

        parent.AddColumn(text=u'Field')
        parent.AddColumn(text=u'Type')
        parent.AddColumn(text=u'Count')
        parent.AddColumn(text=u'Size')
        parent.AddColumn(text=u'Content')

    def _init_utils(self):
        # generated method, don't edit
        self.menuBar = wx.MenuBar()

        self.fileMenu = wx.Menu(title='')

        self.helpMenu = wx.Menu(title='')

        self._init_coll_menuBar_Menus(self.menuBar)
        self._init_coll_fileMenu_Items(self.fileMenu)
        self._init_coll_helpMenu_Items(self.helpMenu)

    def _init_sizers(self):
        # generated method, don't edit
        self.mainFrameSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_mainFrameSizer_Items(self.mainFrameSizer)

        self.SetSizer(self.mainFrameSizer)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_MSGEXPLORERFRAME,
              name=u'MsgExplorerFrame', parent=prnt, pos=wx.Point(366, 204),
              size=wx.Size(875, 761), style=wx.DEFAULT_FRAME_STYLE,
              title=u'MessageExplorer')
        self._init_utils()
        self.SetClientSize(wx.Size(867, 727))
        self.SetMenuBar(self.menuBar)
        self.Center(wx.BOTH)
        self.SetIcon(wx.Icon(u'bitmaps/MsgExplorer.ico',wx.BITMAP_TYPE_ICO))

        self.toolBar1 = wx.ToolBar(id=wxID_MSGEXPLORERFRAMETOOLBAR1,
              name='toolBar1', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(867, 27), style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.SetToolBar(self.toolBar1)

        self.statusBar = wx.StatusBar(id=wxID_MSGEXPLORERFRAMESTATUSBAR,
              name=u'statusBar', parent=self, style=wx.ST_SIZEGRIP)
        self.SetStatusBar(self.statusBar)

        self.mainSplitter = wx.SplitterWindow(id=wxID_MSGEXPLORERFRAMEMAINSPLITTER,
              name=u'mainSplitter', parent=self, point=wx.Point(0, 0),
              size=wx.Size(867, 657), style=wx.SP_3D)
        self.mainSplitter.SetMinimumPaneSize(40)

        self.msgTree = wx.gizmos.TreeListCtrl(id=wxID_MSGEXPLORERFRAMEMSGTREE,
              name=u'msgTree', parent=self.mainSplitter, pos=wx.Point(0, 0),
              size=wx.Size(867, 500),
              style=wx.TR_LINES_AT_ROOT | wx.TR_HAS_BUTTONS)
        self._init_coll_msgTree_Columns(self.msgTree)

        self.logTextCtrl = wx.TextCtrl(id=wxID_MSGEXPLORERFRAMELOGTEXTCTRL,
              name=u'logTextCtrl', parent=self.mainSplitter, pos=wx.Point(0,
              500), size=wx.Size(867, 157),
              style=wx.TE_AUTO_URL | wx.TE_MULTILINE, value=u'')
        self.mainSplitter.SplitHorizontally(self.msgTree, self.logTextCtrl, 500)

        self._init_coll_toolBar1_Tools(self.toolBar1)

        self._init_sizers()

    def __init__(self, parent):
        self.config = wx.ConfigBase_Get()
        self._init_ctrls(parent)

        # Loading file history
        self.filehistory = wx.FileHistory()
        self.filehistory.UseMenu(self.fileMenu)
        self.filehistory.Load(self.config)
        wx.EVT_MENU_RANGE(self, wx.ID_FILE1, wx.ID_FILE9, self.OnFileHistory)

        #self.logOutput = wxLogTextCtrl(self.logTextCtrl)
        self.msgTree.SetColumnWidth(0, 250)
        self.msgTree.SetColumnWidth(1, 60)
        self.msgTree.SetColumnWidth(2, 40)
        self.msgTree.SetColumnWidth(3, 50)
        self.msgTree.SetColumnWidth(4, 250)

        dt = MyFileDropTarget(self, self.logTextCtrl)
        self.msgTree.SetDropTarget(dt)


    def OnFileHistory(self, evt):
        # get the file based on the menu ID
        fileNum = evt.GetId() - wx.ID_FILE1
        path = self.filehistory.GetHistoryFile(fileNum)
        self.Load(path)

        # add it back to the history so it will be moved up the list
        self.filehistory.AddFileToHistory(path)
        self.filehistory.Save(self.config)

    def OnOpen(self, event):
        path = self.config.Read('/LastPath', '.')
        name = self.config.Read('/LastFilename', '')
        filter = self.config.ReadInt('/FilterIndex', 0)
        dlg = wx.FileDialog(self, "Choose a file", path, name, "Dump format(*.dump;*.dmp)|*.dump;*.dmp|I-MX tape(*.IMX)|*.IMX|Base64 format (*.ini;*.txt;*.upd;*.ack;*.lst)|*.ini;*.txt;*.upd;*.ack;*.lst", wx.OPEN)
        dlg.SetFilterIndex(filter)
        try:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                path, name = os.path.split(filename)
                self.Load(filename)
                self.config.Write('/LastPath', path)
                self.config.Write('/LastFilename', name)
                self.config.WriteInt('/FilterIndex', dlg.GetFilterIndex())

                # add it to the history
                self.filehistory.AddFileToHistory(filename)
                self.filehistory.Save(self.config)

        except Exception, details:
            DisplayTrace(self.logTextCtrl, details)
            raise

        dlg.Destroy()

    def Load(self, filename):
        wx.BeginBusyCursor()
        try:
            self.msgTree.DeleteAllItems()
            ext = filename[-4:].lower()
            if ext == '.ini' or ext == '.txt' or ext == '.upd' or ext == '.ack' or ext == '.lst':
                msgs = self.LoadBase64Msg(filename)
            else:
                try:
                    msgs = self.LoadBinaryMsg(filename)
                except:
##                    wx.MessageBox('Header of Message not found in first 512 bytes.', 'Bad file format', wx.ICON_ERROR)
                    self.SetStatusText('Header of Message not found in first 512 bytes. Bad file format!')
                    msgs = self.LoadBase64Msg(filename)
            if msgs != None:
                self.SetStatusText(filename + ' loaded!')
                if isinstance(msgs, muscle.Message.Message):
                    self.ParseMsg(msgs)
                else:
                    rootId = self.msgTree.AddRoot('All Messages')
                    for msg in msgs:
                        msgId = self.msgTree.AppendItem(rootId, 'Msg')
                        self.MakeMessageTitle(msgId, msg)
                        self.ParseMsg(msg, parentId = msgId)
        except Exception, details:
            DisplayTrace(self.logTextCtrl, details)
            raise
        wx.EndBusyCursor()

    def LoadBinaryMsg(self, filename):
        file = open(filename, 'rb')
        ##code, size = struct.unpack("<2L", file.read(2*4))
        header = file.read(512)   # Offset
        offset = header.index('00MP')
        file.seek(offset)
        msg = muscle.Message.Message()
        msg.Unflatten(file)
        file.close();
        return msg

    def LoadBase64Msg(self, filename):
        file = open(filename, 'rt')
        found = False
        allBase64 = []
        for line in file:
            if found:
                if line.find("@END base64@") >= 0:
                    allBase64.append(base64str)
                    found = False
                base64str += line.strip()
            elif line.find("@BEGIN base64 - Don't Edit@") >= 0:
                base64str = ''
                found = True
        file.close();
        msgList = []
        for base64str in allBase64:
            buffer = base64.b64decode(base64str, '+-')
            msg = muscle.Message.Message()
            file = StringIO.StringIO(buffer)
            msg.Unflatten(file)
            file.close()
            msgList.append(msg)
        if len(msgList) > 1:
            return msgList
        elif len(msgList) == 1:
            return msgList[0]
        return None

    def ParseMsg(self, message, parentId = None):
        #message.PrintToStream()
        if parentId == None:
            self.msgTree.AddRoot('')
            parentId = self.msgTree.GetRootItem();
            self.MakeMessageTitle(parentId, message)
        names = message.GetFieldNames()
        for name in names:
            type = message.GetFieldType(name)
            typeStr = muscle.GetHumanReadableTypeString(type)
            typeFriendlyStr = friendlyTypes[type]
            content = message.GetFieldContents(name)
            fieldId = self.msgTree.AppendItem(parentId, name)
            self.msgTree.SetItemText(fieldId, typeFriendlyStr, 1)
            self.msgTree.SetItemText(fieldId, str(len(content)), 2)
            self.msgTree.SetItemText(fieldId, str(message.GetFieldContentsLength(type, content)), 3)
            if type != muscle.B_MESSAGE_TYPE:
                if len(content) == 1:
                    self.msgTree.SetItemText(fieldId, str(content[0]), 4)
                else:
                    self.msgTree.SetItemText(fieldId, repr(content), 4)
            if len(content) > 1 or type == muscle.B_MESSAGE_TYPE or type == muscle.B_RAW_TYPE:
                for value in content:
                    if type == muscle.B_MESSAGE_TYPE:
                        msgId = self.msgTree.AppendItem(fieldId, '')
                        self.MakeMessageTitle(msgId, value)
                        self.ParseMsg(value, msgId)
                    elif type == muscle.B_RAW_TYPE:
                        self.msgTree.AppendItem(fieldId, repr(value))
                    else:
                        self.msgTree.AppendItem(fieldId, str(value))




    def MakeMessageTitle(self, id, msg):
        self.msgTree.SetItemText(id, "Message: what = " + repr(muscle.GetHumanReadableTypeString(msg.what)))
        self.msgTree.SetItemText(id, 'Message', 1)
##        self.msgTree.SetItemText(id, '1', 2)
        self.msgTree.SetItemText(id, str(msg.FlattenedSize()), 3)
        self.msgTree.SetItemText(id, 'what = 0x%X' % msg.what, 4)
##        self.msgTree.SetItemBold(id)

    def GetFriendlyTypeName(self, fieldTypeCode):
        '''Not used'''
        if fieldTypeCode == muscle.B_BOOL_TYPE:
            return 'Boolean'
        elif fieldTypeCode == muscle.B_DOUBLE_TYPE:
            return 'Boolean'
        elif fieldTypeCode == muscle.B_FLOAT_TYPE:
            return 'Boolean'
        elif fieldTypeCode == muscle.B_INT32_TYPE:
            return 'Boolean'
        elif fieldTypeCode == muscle.B_INT16_TYPE:
            return 'Boolean'
        elif fieldTypeCode == muscle.B_INT8_TYPE:
            return 'Boolean'
        elif fieldTypeCode == muscle.B_INT64_TYPE:
            global _dataNeedsSwap, _hasStruct64
            fieldContents = []
            if _hasStruct64:
                for x in range(fieldDataLength/8):
                    fieldContents.append(struct.unpack("<q", file.read(8)))
            else:
                # Old versions of Python don't have <q, so we'll do it ourself
                for x in range(fieldDataLength/8):
                    lo, hi = struct.unpack('<Ll', file.read(8))
                    fieldContents.append((long(hi)<<32)|lo)
        elif fieldTypeCode == B_MESSAGE_TYPE:
            fieldContents = []
            while fieldDataLength > 0:
                subMessageLength = struct.unpack("<L", file.read(4))[0]
                subMsg = Message()
                subMsg.Unflatten(file)
                fieldContents.append(subMsg)
                fieldDataLength = fieldDataLength-(subMessageLength+4)
        elif fieldTypeCode == B_POINT_TYPE:
            fieldContents = []
            for x in range(fieldDataLength/8):
                fieldContents.append(struct.unpack("<2f", file.read(8)))
        elif fieldTypeCode == B_RECT_TYPE:
            fieldContents = []
            for x in range(fieldDataLength/16):
                fieldContents.append(struct.unpack("<4f", file.read(16)))
        elif fieldTypeCode == B_STRING_TYPE:
            fieldContents = []
            numItems = struct.unpack("<L", file.read(4))[0]
            for x in range(numItems):
                fieldContents.append(file.read(struct.unpack("<L", file.read(4))[0]-1))
                file.read(1)  # throw away the NUL byte, we don't need it
        elif fieldTypeCode == B_OBJECT_TYPE or fieldTypeCode == B_RAW_TYPE:
            fieldContents = []
            numItems = struct.unpack("<L", file.read(4))[0]
            for x in range(numItems):
                fieldContents.append(file.read(struct.unpack("<L", file.read(4))[0]))
        else:
            raise "Bad object type encountered in Unflatten() : " + GetHumanReadableTypeString(fieldTypeCode)

    def OnFileMenuOpenMenu(self, event):
        event.Skip()

    def OnFileMenuQuitMenu(self, event):
        self.Close()

    def OnHelpMenuAboutMenu(self, event):
        dlg = AboutDialog.create(self)
        try:
            dlg.ShowModal()
        finally:
            dlg.Destroy()

class MyFileDropTarget(wx.FileDropTarget):
    def __init__(self, frame, log):
        wx.FileDropTarget.__init__(self)
        self.frame = frame
        self.log = log

    def OnDropFiles(self, x, y, filenames):
        if len(filenames) >= 1:
            self.frame.Load(filenames[0])

            # add it to the history
            self.frame.filehistory.AddFileToHistory(filenames[0])
            self.frame.filehistory.Save(self.config)
