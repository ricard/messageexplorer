#!/usr/bin/env python
#Boa:App:BoaApp

from wxPython.wx import *
import psyco
import MsgExplorerFrame

modules ={u'AboutDialog': [0, '', u'AboutDialog.py'],
 u'MsgExplorerFrame': [1, 'Main frame of Application', u'MsgExplorerFrame.py'],
 u'setup': [0, '', u'setup.py']}

class BoaApp(wxApp):
    def OnInit(self):
        wxInitAllImageHandlers()
        self.SetAppName('MsgExplorer')
        self.SetVendorName('SoftArchi')
        self.config = wxConfig()
        wxConfigBase_Set(self.config)

        self.main = MsgExplorerFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        psyco.full()
        return True

def main():
    application = BoaApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
